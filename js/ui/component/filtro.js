// Definimos la variable tabs la cual contendrá todo nuestro modulo.
var filtro = (function () {
    // Objeto la cual establecemos valores que vamos a usar mas adelante en este ámbito.
    var selectores = {
        inputCategoria: ".pnt-js-input-categoria",
        textIdFiltro: ".pnt-js-text-id-filtro",
        textNombreFiltro: ".pnt-js-text-nombre-filtro"
    };

    // Objeto vacío que guardará elementos que se manejan por HTML.
    var dom = {};

    // Función que llenará al objeto dom con los objetos HTML a través de jQuery ($).
    var inicializarDom = function () {
        dom.inputCategoria = $(selectores.inputCategoria);
        dom.textIdFiltro = $(selectores.textIdFiltro);
        dom.textNombreFiltro = $(selectores.textNombreFiltro);
    };

    // Función donde establecemos los eventos que tendrán cada elemento.
    var suscribirEventos = function () {
        dom.inputCategoria.on("change", events.emitirCambioEnFiltro);
    };

    /* Objeto que guarda métodos que se van a usar en cada evento definido 
     en la función suscribeEvents. */
    var events = {
        emitirCambioEnFiltro: function () {
            var nombreDeFiltro = dom.inputCategoria.find(":selected").text();
            var idDeFiltro = dom.inputCategoria.find(":selected").val();
            var filtroSeleccionado = {
                id: idDeFiltro,
                nombre: nombreDeFiltro
            };
            dom.textIdFiltro.trigger("cambiarFiltro", filtroSeleccionado);
            dom.textNombreFiltro.trigger("cambiarFiltro", filtroSeleccionado);
        }
    };  

    // Función que inicializará los funciones decritas anteriormente.
    var init = function () {
        inicializarDom();
        suscribirEventos();
    };

    /* Retorna un objeto literal con el método init haciendo referencia a la 
     función initialize. */
    return{
        init: init
    };
})();

// Ejecutando el método "init" del módulo.
filtro.init();