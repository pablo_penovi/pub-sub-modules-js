// Definimos la variable tabs la cual contendrá todo nuestro modulo.
var idDeFiltro = (function () {
    // Objeto la cual establecemos valores que vamos a usar mas adelante en este ámbito.
    var selectores = {
        textIdFiltro: ".pnt-js-text-id-filtro"
    };

    // Objeto vacío que guardará elementos que se manejan por HTML.
    var dom = {};

    // Función que llenará al objeto dom con los objetos HTML a través de jQuery ($).
    var inicializarDom = function () {
        dom.textIdFiltro = $(selectores.textIdFiltro);
    };

    // Función donde establecemos los eventos que tendrán cada elemento.
    var suscribirEventos = function () {
        dom.textIdFiltro.on("cambiarFiltro", events.cambiarIdDeFiltro);
    };

    /* Objeto que guarda métodos que se van a usar en cada evento definido 
     en la función suscribeEvents. */
    var events = {
        cambiarIdDeFiltro: function (event, filtroSeleccionado) {
            dom.textIdFiltro.text(filtroSeleccionado.id);
        }
    };  

    // Función que inicializará los funciones decritas anteriormente.
    var init = function () {
        inicializarDom();
        suscribirEventos();
    };

    /* Retorna un objeto literal con el método init haciendo referencia a la 
     función initialize. */
    return{
        init: init
    };
})();

// Ejecutando el método "init" del módulo.
idDeFiltro.init();