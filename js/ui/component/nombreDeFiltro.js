// Definimos la variable tabs la cual contendrá todo nuestro modulo.
var nombreDeFiltro = (function () {
    // Objeto la cual establecemos valores que vamos a usar mas adelante en este ámbito.
    var selectores = {
        textNombreFiltro: ".pnt-js-text-nombre-filtro"
    };

    // Objeto vacío que guardará elementos que se manejan por HTML.
    var dom = {};

    // Función que llenará al objeto dom con los objetos HTML a través de jQuery ($).
    var inicializarDom = function () {
        dom.textNombreFiltro = $(selectores.textNombreFiltro);
    };

    // Función donde establecemos los eventos que tendrán cada elemento.
    var suscribirEventos = function () {
        dom.textNombreFiltro.on("cambiarFiltro", events.cambiarNombreDeFiltro);
    };

    /* Objeto que guarda métodos que se van a usar en cada evento definido 
     en la función suscribeEvents. */
    var events = {
        cambiarNombreDeFiltro: function (event, filtroSeleccionado) {
            dom.textNombreFiltro.text(filtroSeleccionado.nombre);
        }
    };  

    // Función que inicializará los funciones decritas anteriormente.
    var init = function () {
        inicializarDom();
        suscribirEventos();
    };

    /* Retorna un objeto literal con el método init haciendo referencia a la 
     función initialize. */
    return{
        init: init
    };
})();

// Ejecutando el método "init" del módulo.
nombreDeFiltro.init();